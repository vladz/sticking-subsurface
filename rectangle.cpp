#include "rectangle.h"

#include <QPainter>

Rectangle::Rectangle(const QColor &color)
    : m_color(color)
{
}

void Rectangle::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    painter.fillRect(QRect(0, 0, width(), height()), m_color);
}
