#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <QRasterWindow>

class Rectangle : public QRasterWindow
{
    Q_OBJECT

public:
    explicit Rectangle(const QColor &color);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QColor m_color;
};

#endif // RECTANGLE_H
