#include "rectangle.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Rectangle mainSurface(Qt::blue);
    mainSurface.setGeometry(0, 0, 800, 600);
    mainSurface.show();

    Rectangle subSurface(Qt::red);
    subSurface.setGeometry(800, 0, 800, 600);
    subSurface.setParent(&mainSurface);
    subSurface.show();

    return a.exec();
}
